import numpy as np
import random


def monte_carlo(f, a, b, N):
	V = b - a
	avrg = 0
	sq_avrg = 0

	for _ in range(N):
		x = random.random() * V + a
		avrg += f(x) / N
		sq_avrg += f(x)**2 / N

	sigma = V * np.sqrt(np.abs(sq_avrg - avrg**2) / (N - 1))

	return avrg * V, sigma


def monte_carlo_importance_inverse_transform(f, a, b, N, g, G_inv):
	V = b - a
	avrg = 0
	sq_avrg = 0

	for _ in range(N):
		x = G_inv(random.random())
		# x = inverse_transform_generation(G, G_inv, a, b)
		avrg += f(x) / g(x) / N
		sq_avrg += (f(x) / g(x))**2 / N

	sigma = V * np.sqrt(np.abs(sq_avrg - avrg**2) / (N - 1))

	return avrg * V, sigma


def gauss(x, mu, sigma):
	return 1 / np.sqrt(2 * np.pi * sigma**2) * np.exp(-(x - mu)**2 / (2 * sigma**2))


def random_custom(distribution):
	while True:
		r1 = random.random()
		r2 = random.random()

		if r2 < distribution(r1):
			return r1


# generate random numbers with custom distribution between a and b, with G = integral(distribution)
# Why does this not work???
def inverse_transform_generation(G, G_inv, a, b):
	return G(G_inv(a) + random.random() * (G(b) - G(a)))
