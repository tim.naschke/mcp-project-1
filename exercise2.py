import numpy as np
import random
import matplotlib.pyplot as plt


def random_walk_step(x, y):
	dx = random.random() * 2 - 1
	dy = random.random() * 2 - 1

	L = np.sqrt(dx**2 + dy**2)
	dx = dx / L
	dy = dy / L

	return x + dx, y + dy


def random_walk(steps):
	x, y = 0, 0
	walk_x = [0]
	walk_y = [0]

	for _ in range(steps):
		x, y = random_walk_step(x, y)
		walk_x.append(x)
		walk_y.append(y)

	return walk_x, walk_y


# a) 4 Random Walks
for i in range(1, 4):
	x, y = random_walk(1000)
	plt.plot(x, y, label=f"Run #{i}")

plt.xlabel("x")
plt.ylabel("y")
plt.grid(linestyle="--")
plt.legend()
plt.savefig("2a.png")
plt.show()


# b) Histogramm
N = 10000
M = 1000

res = []
for _ in range(M):
	x, y = random_walk(N)
	res.append(np.sqrt(x[-1]**2 + y[-1]**2))

plt.xlabel(r"distance from origin $R_N$")
plt.ylabel("count")
plt.hist(res, 40)
plt.savefig("2b.png")
plt.show()

res = np.array(res)
rms = np.sqrt(np.mean(res**2))
print("RMS:", rms)
print("RMS theoretical:", np.sqrt(N))
