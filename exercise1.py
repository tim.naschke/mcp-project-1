import numpy as np

from monte_carlo import *
import matplotlib.pyplot as plt


def f(x):
	return x**4


def g1(x):
	return 2 * x


def g2(x):
	return 3 * x**2


def g3(x):
	return 4 * x**3


def g4(x):
	return 5 * x**4


# a)
print(monte_carlo(f, 0, 1, 1000))


# b) Histogramm
res = []

for _ in range(1000):
	res.append(monte_carlo(f, 0, 1, 1000)[0])

avrg = np.mean(res)
sigma = np.std(res)

fig = plt.figure(dpi=150)
plt.hist(res, 50, label="numerical")

x = np.linspace(min(res), max(res), 200)
plt.plot(x, gauss(x, avrg, sigma), label="gaussian fit")

plt.xlabel("I(N)")
plt.ylabel("count")
plt.legend()
plt.savefig("1b.png")
plt.show()

print("result from gaussian distribution:", avrg)
print("sigma_N from gaussian distribution:", sigma)


# c)
print("\nImportance Sampling:")

g = [lambda x: 2 * x, lambda x: 3 * x**2, lambda x: 4 * x**3, lambda x: 5 * x**4]
# G = [lambda x: x**2, lambda x: x**3, lambda x: x**4, lambda x: x**5]
G_inv = [lambda x: x**(1/2), lambda x: x**(1/3), lambda x: x**(1/4), lambda x: x**(1/5)]

for i in range(len(g)):
	print(monte_carlo_importance_inverse_transform(f, 0, 1, 1000, g[i], G_inv[i]))

labels = ["2x", "3x²", "4x³", "5x⁴"]
N_range = [10, 100, 1000, 10000, 100000, 1000000]
for i in range(len(g)):
	sigma_res = []

	for N in N_range:
		sigma_res.append(monte_carlo_importance_inverse_transform(f, 0, 1, N, g[i], G_inv[i])[1])

	plt.loglog(N_range, sigma_res, label=labels[i])

plt.xlabel("N")
plt.ylabel(r"$\sigma_N$")
plt.grid(linestyle="--")
plt.legend()
plt.savefig("1c.png")
plt.show()
