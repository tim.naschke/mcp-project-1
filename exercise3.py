import numpy as np
import random
import matplotlib.pyplot as plt


def random_walk_step(x, y, grid):
	directions = ["N", "O", "S", "W"]

	if y >= 31 or grid[x][y+1] != 0:
		directions.remove("N")
	if x >= 31 or grid[x+1][y] != 0:
		directions.remove("O")
	if y <= 1 or grid[x][y-1] != 0:
		directions.remove("S")
	if x <= 1 or grid[x-1][y] != 0:
		directions.remove("W")

	if not directions:
		return None

	direction = random.choice(directions)

	if direction == "N":
		return x, y + 1
	if direction == "O":
		return x + 1, y
	if direction == "S":
		return x, y - 1
	if direction == "W":
		return x - 1, y


def count_h_neighbors(x, y, grid):
	h_neighbors = 0

	if not y >= 31 and grid[x][y + 1] == 1:
		h_neighbors += 1
	if not x >= 31 and grid[x + 1][y] == 1:
		h_neighbors += 1
	if not y <= 1 and grid[x][y - 1] == 1:
		h_neighbors += 1
	if not x <= 1 and grid[x - 1][y] == 1:
		h_neighbors += 1

	return h_neighbors


def random_walk():
	grid = np.array([[0] * 32] * 32)
	pH = 0.7
	pP = 1 - pH

	walk_x = [16]
	walk_y = [16]
	h_pairs = 0

	if random.random() < pH:
		grid[16][16] = 1		# 1: H
	else:
		grid[16][16] = 2		# 2: P

	while True:
		try:
			x, y = random_walk_step(walk_x[-1], walk_y[-1], grid)
		except TypeError:
			break

		walk_x.append(x)
		walk_y.append(y)

		if random.random() < pH:
			grid[x][y] = 1		# 1: H

			h_pairs += count_h_neighbors(walk_x[-1], walk_y[-1], grid)
			if grid[walk_x[-2]][walk_y[-2]] == 1:
				h_pairs -= 1
		else:
			grid[x][y] = 2		# 2: P

	return h_pairs, len(walk_x) - 1, walk_x, walk_y


# f, L, x, y = random_walk()
# plt.plot(x, y)
# plt.xlim(0.5, 31.5)
# plt.ylim(0.5, 31.5)
# plt.show()

# print("H-H Pairs which are not chained:", f)
# print("Length:", L)


M = 1000
results = []
for _ in range(1000):
	results.append(random_walk())

results = np.array(results)
f = results[:, 0]
L = results[:, 1]
x = results[:, 2]
y = results[:, 3]

plt.hist(-f, 50)
plt.xlabel("Energy")
plt.ylabel("count")
plt.savefig("3_energy.png")
plt.show()

plt.hist(L, 50)
plt.xlabel("Length")
plt.ylabel("count")
plt.savefig("3_length.png")
plt.show()

index_max = np.where(f == max(f))[0][0]
index_min = np.where(f == min(f))[0][0]
index_mid = np.sort(f)[int(M/2)]

plt.plot(x[index_max], y[index_max])
plt.title("minimal Energy")
plt.xlim(0.5, 31.5)
plt.ylim(0.5, 31.5)
plt.savefig("3_energy_min.png")
plt.show()

plt.plot(x[index_min], y[index_min])
plt.title("maximal Energy")
plt.xlim(0.5, 31.5)
plt.ylim(0.5, 31.5)
plt.savefig("3_energy_max.png")
plt.show()

plt.plot(x[index_mid], y[index_mid])
plt.title("median Energy")
plt.xlim(0.5, 31.5)
plt.ylim(0.5, 31.5)
plt.savefig("3_energy_mid.png")
plt.show()

plt.hist2d(-f, L, 50)
plt.xlabel("Energy")
plt.ylabel("Length")
plt.savefig("3_el_hist.png")
plt.show()
