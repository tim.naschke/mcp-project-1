import random
import matplotlib.pyplot as plt
import numpy as np


def decay(N, l, delta_t):
	N = [N]
	t = [0]

	while N[-1] > 0:
		decayed = 0

		for _ in range(N[-1]):
			if random.random() < l * delta_t:
				decayed += 1

		t.append(t[-1] + delta_t)
		N.append(N[-1] - decayed)

	return t, N


# Parameters
N_0 = [10, 100, 1000, 10000, 100000]
l = 0.03
delta_t = 1
# --------

for start_N in N_0:
	t, N = decay(start_N, l, delta_t)
	plt.plot(t, N, label=f"N(0) = {start_N}")

	x = np.linspace(min(t), max(t), 200)
	plt.plot(x, start_N * np.exp(-l * x), label=f"N(0) = {start_N} | Theory")

	plt.xlabel("t [s]")
	plt.ylabel("N(t)")
	plt.legend()
	plt.savefig(f"4a_{start_N}_{l}.png")
	plt.show()
