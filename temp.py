from monte_carlo import *
import matplotlib.pyplot as plt


def distribution(x):
	return 4 * (x-0.5)**3 + 1


res = []
for i in range(1000):
	res.append(random_custom(distribution))

fig = plt.figure(dpi=150)
plt.hist(res, 30)
plt.show()


grid = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
grid = np.array(grid)
print(grid[:, 1])
